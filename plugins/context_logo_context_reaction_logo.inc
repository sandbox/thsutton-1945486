<?php
/**
 * @file
 *
 * Context reaction plugin to set the site logo theme variable.
 */

/**
 * Context reaction plugin to override the site logo.
 */
class context_logo_context_reaction_logo extends context_reaction {

  /**
   * Configuration form.
   *
   * @param $context
   *     Context being configured.
   * @return array
   *     Form.
   */
  function options_form($context) {
    $theme_settings = variable_get('theme_settings', array());
    $value = $this->fetch_from_context($context);
    return array(
      'path' => array(
        '#type' => 'textfield',
        '#title' => t('Path to custom logo'),
        '#description' => t('The path to the file you would like to use as your logo file instead of the default logo.'),
        '#required' => TRUE,
        '#default_value' => empty($value) ? $theme_settings['logo_path'] : $value,
      ),
    );
  }

  /**
   * Process configuration form submission.
   *
   * @param  array $values
   *     Values submitted.
   * @return string
   *     Values to save.
   */
  function options_form_submit($values) {
    return $values['path'];
  }

  /**
   * Override the logo_path variable
   */
  function execute(&$vars = NULL) {
    $classes = array();
    foreach ($this->get_contexts() as $k => $v) {
      if (!empty($v->reactions[$this->plugin])) {
        $vars['logo'] = file_create_url($v->reactions[$this->plugin]);
      }
    }
  }
}
